# -*- mode: ruby -*-
# vi: set ft=ruby :
# For windows, install git bash - https://git-for-windows.github.io
# Install VirtualBox - https://www.virtualbox.org/wiki/Downloads
# Install Vagrant - https://www.vagrantup.com
# Add "192.168.99.100 vagrant" to hosts file
# Run "vagrant up" in editpyxl folder

Vagrant.configure("2") do |django_config|
  # For more info, see online documentation at https://docs.vagrantup.com.

  # Base box to build off, and download URL for when it doesn't exist on the user's system already
  # Find boxes at https://atlas.hashicorp.com/search.
  # -- ubuntu/xenial64 has issues
  # django_config.vm.box = "ubuntu/xenial64"
  # geerlingguy/ubuntu1604 is missing core elements, which can be installed, but then 16.04 has issues installing Mapnik
  # django_config.vm.box = "geerlingguy/ubuntu1604"
  django_config.vm.box = "ubuntu/trusty64"
  # This box requires:
  # apt-get install apache2
  # apt install virtualenv

  # Configure virtual machine specs. Keep it simple, single user.
  django_config.vm.provider :virtualbox do |p|
    p.customize ["modifyvm", :id, "--memory", 1024]
    p.customize ["modifyvm", :id, "--cpus", 2]
    p.customize ["modifyvm", :id, "--cpuexecutioncap", 50]
  end

  # Configure a synced folder between HOST and GUEST
  django_config.vm.synced_folder ".", "/var/dev/src/editpyxl", id: "vagrant-root", :mount_options => ["dmode=777","fmode=777"]

  # Config hostname and IP address so entry can be added to HOSTS file
  django_config.vm.hostname = "vagrant"
  django_config.vm.network :private_network, ip: '192.168.99.100'

  # Forward a port from the guest to the host, which allows for outside
  # computers to access the VM, whereas host only networking does not.
  django_config.vm.network "forwarded_port", guest: 80, host: 8080
  django_config.vm.network "forwarded_port", guest: 443, host: 8443
  django_config.vm.network "forwarded_port", guest: 8000, host: 8000

  # If true, then any SSH connections made will enable agent forwarding.
  # Default value: false
  django_config.ssh.forward_agent = true

  # kickoff a shell script to install Python essentials
  django_config.vm.provision :shell, path: "vagrant-bootstrap.sh"
end